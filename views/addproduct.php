<!doctype html>
<html lang="en">
    <head>
        <title>Products | Add</title>

        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>

        <link rel="stylesheet" href="/app.css">

        <script src="main.js"></script>

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@400;700&family=Bellefair&family=Barlow:wght@400;700&display=swap">
    </head>
    <body class="bg-white text-dark">
        <div class="container" style="padding-bottom: 10rem;">
            <form id="product_form" action="/addproduct" method="POST">
                <ul class="header">
                    <li><h2>Product Add</h2></li>
                    <li class="right"><a href="/" class="button">Cancel</a></li>
                    <li class="right"><button type="submit" class="button" style="background-color: hsl(var(--clr-green));">Save</button></li>
                </ul>

                <hr>

                <?php if (!empty($errors)): ?>
                <div class="errors text-white">
                    <?php foreach($errors as $error): ?>
                    <p><?php echo $error; ?></p>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>

                <div class="form">
                    <div class="inputs grid">
                        <div class="flex">
                            <label for="sku">SKU</label>
                            <input id="sku" type="text" name="sku" required>
                        </div>

                        <div class="flex">
                            <label for="name">Name</label>
                            <input id="name" type="text" name="name" required>
                        </div>

                        <div class="flex">
                            <label for="price">Price ($)</label>
                            <input id="price" type="number" name="price" min="0.01" step="0.01" required>
                        </div>
                    </div>

                    <div class="selector flex">
                        <label for="productType">Type switcher</label>
                        <select id="productType">
                            <option value="dvd">DVD</option>
                            <option value="book">Book</option>
                            <option value="furniture">Furniture</option>
                        </select>
                    </div>

                    <div id="change" class="inputs grid">
                        <div class="flex">
                            <label for="size">Size (MB)</label>
                            <input id="size" type="number" min="1" name="size" required>
                        </div>

                        <p id="description" class="text-green">Please provide a size in MB of the DVD</p>
                    </div>
                </div>

                <hr>
            </form>
        </div>

        <footer class="flex bg-dark text-white">
            Scandiweb Test assignment
        </footer>
    </body>
</html>