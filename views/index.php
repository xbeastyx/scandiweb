<!doctype html>
<html lang="en">
    <head>
        <title>Products | Home</title>

        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>

        <link rel="stylesheet" href="/app.css">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@400;700&family=Bellefair&family=Barlow:wght@400;700&display=swap">
    </head>
    <body class="bg-white text-dark">
        <div class="container" style="padding-bottom: 10rem;">
            <form action="/delete" method="POST">
                <ul class="header">
                    <li><h2>Product List</h2></li>
                    <li class="right"><button type="submit" id="button">MASS DELETE</button></li>
                    <li class="right"><a id="button" href="/addproduct" style="background-color: hsl(var(--clr-green));">ADD</a></li>
                </ul>

                <hr>

                <div class="products flex">
                    <?php foreach($products as $product): ?>
                    <div class="product">
                        <input class="delete-checkbox" name="<?php echo $product['sku'] ?>" type="checkbox">

                        <div class="product-info">
                            <h5><?php echo $product['sku'] ?></h6>
                            <p><?php echo $product['name'] ?></p>
                            <p><?php echo $product['price'] ?> $</p>
                            <p><?php echo $product['attribute'] ?></p>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>

                <hr>
            </form>
        </div>

        <footer class="flex bg-dark text-white">
            Scandiweb Test assignment
        </footer>
    </body>
</html>