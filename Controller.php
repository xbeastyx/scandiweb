<?php

namespace app;

use app\Router;
use app\models\DVD;
use app\models\Book;
use app\models\Furniture;

class Controller {
    public static function index(Router $router) {
        $products = $router->db->getProducts();

        $router->renderView('index', [
            'products' => $products
        ]);
    }

    public static function add(Router $router) {
        $router->renderView('addproduct', []);
    }

    public static function create(Router $router) {
        $data = [
            'sku' => $_POST['sku'],
            'name' => $_POST['name'],
            'price' => $_POST['price'],
            'attribute' => ''
        ];

        $checkProduct = $router->db->getProduct($data['sku']);
        $errors = [];

        if (!empty($checkProduct)) {
            $errors[] = 'Product with specified SKU already exists!';

            $router->renderView('addproduct', [
                'errors' => $errors
            ]);
        } else {
            $size = $_POST['size'] ?? null;
            $weight = $_POST['weight'] ?? null;
            $height = $_POST['height'] ?? null;
            $width = $_POST['width'] ?? null;
            $length = $_POST['length'] ?? null;

            if ($size) {
                $data['size'] = $size;

                $product = new DVD($data);
            }

            if ($weight) {
                $data['weight'] = $weight;

                $product = new Book($data);
            }

            if ($height && $width && $length) {
                $data['height'] = $height;
                $data['width'] = $width;
                $data['length'] = $length;

                $product = new Furniture($data);
            }

            $product->save();

            header('Location: /');
        }
    }

    public static function delete(Router $router) {
        foreach ($_POST as $sku => $val) {
            $router->db->deleteProduct($sku);
        }

        header('Location: /');
    }
}