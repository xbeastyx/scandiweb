<?php

namespace app\models;

use app\models\Product;
use app\Database;

class Book extends Product {
    public ?float $weight = null;

    public function __construct($data) {
        parent::__construct($data);
        $this->weight = $data['weight']; 
    }

    public function save() {
        $db = Database::$db;

        $db->addBook($this);
    }
}