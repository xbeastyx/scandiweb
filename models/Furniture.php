<?php

namespace app\models;

use app\models\Product;
use app\Database;

class Furniture extends Product {
    public ?int $height = null;
    public ?int $width = null;
    public ?int $length = null;

    public function __construct($data) {
        parent::__construct($data);
        $this->height = $data['height'];
        $this->width = $data['width'];
        $this->length = $data['length'];
    }

    public function save() {
        $db = Database::$db;

        $db->addFurniture($this);
    }
}