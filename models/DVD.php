<?php

namespace app\models;

use app\models\Product;
use app\Database;

class DVD extends Product {
    public ?int $size = null;

    public function __construct($data) {
        parent::__construct($data);
        $this->size = $data['size']; 
    }

    public function save() {
        $db = Database::$db;

        $db->addDVD($this);
    }
}