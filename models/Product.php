<?php

namespace app\models;

abstract class Product {
    public ?string $sku = null;
    public ?string $name = null;
    public ?float $price = null;

    public function __construct($data) {
        $this->sku = $data['sku'];
        $this->name = $data['name'];
        $this->price = $data['price'];
    }
}