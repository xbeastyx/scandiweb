<?php

require_once __DIR__.'/../vendor/autoload.php';

use app\Controller;
use app\Router;

$router = new Router();

$router->get('/', [Controller::class, 'index']);
$router->get('/addproduct', [Controller::class, 'add']);
$router->post('/addproduct', [Controller::class, 'create']);
$router->post('/delete', [Controller::class, 'delete']);

$router->resolve();