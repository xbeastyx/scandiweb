window.addEventListener('DOMContentLoaded', () => {
    init();
});

function init() {
    const switcher = document.querySelector('#productType');

    switcher.addEventListener('change', () => {
        updateRender(switcher.value);
    });
}

function updateRender(type) {
    const change = document.querySelector('#change');
    const description = document.querySelector('#description');

    clearElement(change);

    const flexDIV = document.createElement('div');
    flexDIV.classList.add('flex');

    if (type === 'book') {
        const label = document.createElement('label');
        label.setAttribute('for', 'weight');
        label.innerHTML = 'Weight (KG)';

        const input = newInput('weight', 'number', '0.1', 'weight', '0.1');

        flexDIV.append(label);
        flexDIV.append(input);

        change.prepend(flexDIV);
        
        description.innerHTML = 'Please provide a weight in KG of the Book';
    }

    if (type === 'furniture') {
        const label = document.createElement('label');
        label.setAttribute('for', 'height');
        label.innerHTML = 'Height (CM)';

        const label2 = document.createElement('label');
        label2.setAttribute('for', 'width');
        label2.innerHTML = 'Width (CM)';

        const label3 = document.createElement('label');
        label3.setAttribute('for', 'length');
        label3.innerHTML = 'Length (CM)';

        const input = newInput('height', 'number', '1', 'height');
        const input2 = newInput('width', 'number', '1', 'width');
        const input3 = newInput('length', 'number', '1', 'length');

        flexDIV.append(label3);
        flexDIV.append(input3);

        change.prepend(flexDIV);

        const flexDIV2 = document.createElement('div');
        flexDIV2.classList.add('flex');

        flexDIV2.append(label2);
        flexDIV2.append(input2);

        change.prepend(flexDIV2);

        const flexDIV3 = document.createElement('div');
        flexDIV3.classList.add('flex');

        flexDIV3.append(label);
        flexDIV3.append(input);
        change.prepend(flexDIV3);

        description.innerHTML = 'Please provide dimensions in HxWxL format for the Furniture';
    }

    if (type === 'dvd') {
        const label = document.createElement('label');
        label.setAttribute('for', 'size');
        label.innerHTML = 'Size (MB)';

        const input = newInput('size', 'number', '1', 'size');

        flexDIV.append(label);
        flexDIV.append(input);

        change.prepend(flexDIV);

        description.innerHTML = 'Please provide a size in MB of the DVD';
    }
}

function clearElement(element) {
    while (element.firstElementChild.nodeName === 'DIV') {
        element.removeChild(element.firstElementChild);
    }
}

function newInput(id, type, min, name, step) {
    const input = document.createElement('input');

    input.setAttribute('id', id);
    input.setAttribute('type', type);
    input.setAttribute('min', min);
    input.setAttribute('name', name);
    input.setAttribute('required', '');
    if (step) input.setAttribute('step', step);

    return input;
}