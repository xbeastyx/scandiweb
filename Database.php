<?php

namespace app;

use PDO;
use app\models\Product;

class Database {
    public $pdo;
    public static Database $db;

    public function __construct() {
        $servername = 'localhost';
        $username = 'root';
        $password = '';
        $database = 'products';

        $this->pdo = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        self::$db = $this;
    }

    public function getProducts() {
        $statement = $this->pdo->prepare('SELECT * FROM products ORDER BY sku ASC');

        $statement->execute();
        
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProduct($sku) {
        $statement = $this->pdo->prepare('SELECT * FROM products WHERE sku = :sku');

        $statement->bindValue(':sku', $sku);

        $statement->execute();
        
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addBook(Product $product) {
        $statement = $this->pdo->prepare("INSERT INTO products (sku, name, price, attribute) VALUES (:sku, :name, :price, :attribute)");

        $statement->bindValue(':sku', $product->sku);
        $statement->bindValue(':name', $product->name);
        $statement->bindValue(':price', $product->price);
        $statement->bindValue(':attribute', "Weight: $product->weight".'KG');

        $statement->execute();
    }

    public function addDVD(Product $product) {
        $statement = $this->pdo->prepare("INSERT INTO products (sku, name, price, attribute) VALUES (:sku, :name, :price, :attribute)");

        $statement->bindValue(':sku', $product->sku);
        $statement->bindValue(':name', $product->name);
        $statement->bindValue(':price', $product->price);
        $statement->bindValue(':attribute', "Size: $product->size MB");

        $statement->execute();
    }

    public function addFurniture(Product $product) {
        $statement = $this->pdo->prepare("INSERT INTO products (sku, name, price, attribute) VALUES (:sku, :name, :price, :attribute)");

        $statement->bindValue(':sku', $product->sku);
        $statement->bindValue(':name', $product->name);
        $statement->bindValue(':price', $product->price);
        $statement->bindValue(':attribute', 'Dimension: '.$product->height.'x'.$product->width.'x'.$product->length);

        $statement->execute();
    }

    public function deleteProduct($sku) {
        $statement = $this->pdo->prepare('DELETE FROM products WHERE sku = :sku');

        $statement->bindValue(':sku', $sku);

        $statement->execute();
    }
}